import json

import pytest
from tornado.testing import AsyncHTTPTestCase

from app import get_app


@pytest.fixture
def app():
    return get_app()


@pytest.mark.gen_test
def test_get(http_client, base_url):
    response = yield http_client.fetch(base_url, raise_error=False)
    assert response.code == 405


@pytest.mark.gen_test
@pytest.mark.parametrize(
    'lines,status_code,records_valid,records_invalid', 
    [
        ([], 200, 0, 0),
        ([{
            "vendorid": 2,
            "tpep_pickup_datetime": "2017-01-01 00: 00: 02",
            "tpep_dropoff_datetime": "2017-01-01 00: 39: 22",
            "passenger_count": 4,
            "trip_distance": 7.75,
            "ratecodeid": 1,
            "store_and_fwd_flag": "N",
            "pulocationid": 186,
            "dolocationid": 36,
            "payment_type": 1,
            "fare_amount": 22,
            "extra": 0.5,
            "mta_tax": 0.5,
            "tip_amount": 4.66,
            "tolls_amount": 0,
            "improvement_surcharge": 0.3,
            "total_amount": 27.96
        }], 200, 1, 0),
        (['notajson'], 200, 0, 1),
    ]
)
def test_post(
    http_client, base_url, 
    lines, status_code, records_valid, records_invalid
):
    request_body = '\n'.join(json.dumps(line) for line in lines)
    response = yield http_client.fetch(
        base_url, 
        method='POST',
        body=request_body,
        raise_error=False
    )
    assert response.code == status_code
    body = json.loads(response.body)

    assert body == {
        'result': {
            'status': 'ok',
            'stats': {
                'bytes': len(request_body),
                'records': {
                    'valid': records_valid,
                    'invalid': records_invalid,
                    'total': records_valid + records_invalid,
                },
            }
        }
    }
