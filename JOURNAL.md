# Dev Journal

I will be using this file as the main journal to explain my reasoning, what changes I would do and so on.

## Install and initial exploration

The first thing to do was to make sure the code is working, and behaving as expected. Following (some of)
the steps indicated in BUILD.md, I made sure the depdencies were installed, and installed the project. For it,
I used `pyenv`, as it is the tool and most familiar with:

```
➜  coruscant git:(main) pyenv virtualenv 3.10.2 coruscant
➜  coruscant git:(main) pyenv activate coruscant
(coruscant) ➜  coruscant git:(main) pip install -e .
```

This worked without issue, and launching the server to test it didn't have any issues: it ran perfectly,
plus the example worked without any issues.

The code seemed simple enough, but before venturing to make any substantial modifcation, I decided to write some
simple tests. I know no better way of working with legacy/unknown code other than wrapping the logic around tests.

## Tests

Using pytest (and pytest-tornado) I added some very basic tests. At first, they do not check the contents of the file
but they gave me some room to play with. To run the tests, we need to install the requirements:

```
pip install -r test_requirements.txt
pytest .
```

Ideally, I'd mock the method `writerow` from the `csv.DictWriter` to check with what values we are calling it. However
I was running into some issues with tornado + pytest + patch that I decided not to spend too much time on.
Instead, I am reading the file.

## Issues and changes

At a first glance, the approach I'd follow to optimise it would be to parallelize somehow generating the output data.

The first issue seems to be the output file itself. Every single request would write to the same file, as we are
only using today's date as the naming factor for the file. If we do not care about the data being partitioned into
different files (rather than just one per day), it would simplify this parallelization.

Decided to use Python's multiprocessing library to split said generating of the files, but the first attempt did not
move the benchmark very much. In addition, I changed it so we write the lines after processing, not keeping the file opened
all the time.

As this was going nowhere, I thought it was better got go back to basis. The main bottleneck is still writing the file,
but maybe writing into the same file is not that much of an issue. Making the most of the async nature of Tornado, plus
Python's built-in async functionality, I made the `writerow` method non-blocker with `await` and that significantly
improved the timings.

Annoyingly this broke the tests, but at this point I decided it was enough for the exploration, and in a real scenario
I would see why adding the `async` keyword made the test break.

## Potential improvements

If I were to improve this server, I'd follow the same line as above: trying to distribute/parallelize writing the result
so it's better distributed across different processes. Probably also trying to see if we can follow a similar pattern for
reading the input, given we know each row is (should be) a complete JSON.

In terms of "quality of life" improvements, I'd of course add some linting and improve the tests so they
are actually testing things.
